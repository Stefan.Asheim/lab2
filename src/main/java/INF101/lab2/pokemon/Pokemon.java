package INF101.lab2.pokemon;
import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random = new Random();

    ///// Oppgave 1b
    public Pokemon (String name, int healthPoints, int strength)
    {
        this.name = name;
        this.healthPoints = healthPoints;
        maxHealthPoints = healthPoints;
        if (maxHealthPoints < 1)
        {
            throw new IllegalArgumentException("Max health must be greater than 0");
        }
        if(strength <1)
        {
            throw new IllegalArgumentException("Strength must be greater than 0");
        }
        this.strength = strength;
        
    }

    ///// Oppgave 2
	/**
     * Get name of the pokémon
     * @return name of pokémon
     */
    String getName() {        
        return name;
    }

    /**
     * Get strength of the pokémon
     * @return strength of pokémon
     */
    int getStrength() {
        return strength;
    }

    /**
     * Get current health points of pokémon
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return healthPoints;
    }
    
    /**
     * Get maximum health points of pokémon
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return maxHealthPoints;
    }

    /**
     * Check if the pokémon is alive. 
     * A pokemon is alive if current HP is higher than 0
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        if(healthPoints > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    
    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        if(getCurrentHP() - damageTaken <= 0)
        {
            healthPoints = 0;
        }
        else
        {
            healthPoints -= Math.max(damageTaken,0);
        }
        System.out.println(String.format("%s takes %d damage and is left with %d/%d HP", name, damageTaken, getCurrentHP(), maxHealthPoints));
    }

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        String targetName = target.getName();
        System.out.println(String.format("%s attacks %s", getName(), targetName));
        int damage = random.nextInt(2)*getStrength();
        target.damage(damage);


    }

    ///// Oppgave 3
    @Override
    public String toString() {
        return String.format("%s HP: (%d/%d) STR: %d", name, getCurrentHP(), maxHealthPoints, strength);
    }

}
